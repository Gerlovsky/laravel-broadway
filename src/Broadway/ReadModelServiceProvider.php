<?php namespace Nodesky\LaravelBroadway\Broadway;

use Illuminate\Support\ServiceProvider;

class ReadModelServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            \Nodesky\LaravelBroadway\ReadModel\ReadModelFactory::class,
            \Nodesky\LaravelBroadway\ReadModel\Broadway\BroadwayReadModelFactory::class
        );

        $driver = $this->app['config']->get('broadway.read-model');

        $this->app->singleton(ucfirst($driver), function ($app) use ($driver) {
            return $app[\Nodesky\LaravelBroadway\ReadModel\ReadModelFactory::class]->make($driver)->getDriver();
        });
    }
}
