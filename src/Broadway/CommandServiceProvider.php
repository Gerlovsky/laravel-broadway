<?php namespace Nodesky\LaravelBroadway\Broadway;

use Broadway\CommandHandling\SimpleCommandBus;
use Illuminate\Support\ServiceProvider;
use Nodesky\LaravelBroadway\Registries\CommandRegistry;

class CommandServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(\Broadway\CommandHandling\CommandBus::class, function () {
            return new SimpleCommandBus();
        });

        $this->app->singleton('laravelbroadway.command.registry', function ($app) {
            return new CommandRegistry($app[\Broadway\CommandHandling\CommandBus::class]);
        });
    }
}
