<?php namespace Nodesky\LaravelBroadway\ReadModel;

interface ReadModelFactory
{
    /**
     * @param  string                                    $driver



*
*@return \Nodesky\LaravelBroadway\ReadModel\Driver
     */
    public function make($driver);
}
