<?php namespace Nodesky\LaravelBroadway\ReadModel\Broadway\Drivers;

use Broadway\ReadModel\InMemory\InMemoryRepository;
use Nodesky\LaravelBroadway\ReadModel\Driver;

class Inmemory implements Driver
{
    /**
     * @return object
     */
    public function getDriver()
    {
        return new InMemoryRepository();
    }
}
