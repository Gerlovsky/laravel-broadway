<?php namespace Nodesky\LaravelBroadway\ReadModel\Broadway;

use Nodesky\LaravelBroadway\ReadModel\ReadModelFactory;

class BroadwayReadModelFactory implements ReadModelFactory
{
    /**
     * @param  string                                    $driver
      


*
*@return \Nodesky\LaravelBroadway\ReadModel\Driver
     */
    public function make($driver)
    {
        $driver = 'Nodesky\LaravelBroadway\ReadModel\Broadway\Drivers\\' . ucfirst($driver);

        return new $driver();
    }
}
