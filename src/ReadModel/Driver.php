<?php namespace Nodesky\LaravelBroadway\ReadModel;

interface Driver
{
    /**
     * @return object
     */
    public function getDriver();
}
