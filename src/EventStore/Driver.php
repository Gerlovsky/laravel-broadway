<?php namespace Nodesky\LaravelBroadway\EventStore;

interface Driver
{
    /**
     * @return object
     */
    public function getDriver();
}
