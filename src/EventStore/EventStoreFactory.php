<?php namespace Nodesky\LaravelBroadway\EventStore;

interface EventStoreFactory
{
    /**
     * @param  string $driver


*
*@return \Nodesky\LaravelBroadway\EventStore\Driver
     */
    public function make($driver);
}
