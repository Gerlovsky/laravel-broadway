<?php namespace Nodesky\LaravelBroadway\EventStore\Broadway\Drivers;

use Broadway\EventStore\InMemoryEventStore;
use Nodesky\LaravelBroadway\EventStore\Driver;

class Inmemory implements Driver
{
    /**
     * @return object
     */
    public function getDriver()
    {
        return new InMemoryEventStore();
    }
}
