<?php namespace Nodesky\LaravelBroadway\EventStore\Broadway;

use Nodesky\LaravelBroadway\EventStore\EventStoreFactory;

class BroadwayEventStoreFactory implements EventStoreFactory
{
    /**
     * @param  string $driver


*
*@return \Nodesky\LaravelBroadway\EventStore\Driver
     */
    public function make($driver)
    {
        $driver = 'Nodesky\LaravelBroadway\EventStore\Broadway\Drivers\\' . ucfirst($driver);

        return new $driver();
    }
}
